/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

import java.util.Scanner;

/**
 *
 * @author werapan
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(15, 15);
        Robot robot = new Robot(2, 2, 'x', map, 10);
        Bomb bomb = new Bomb(5,5);
        map.addObj(new Tree(10,10)); //Priority Low
        map.addObj(new Tree(9,10));
        map.addObj(new Tree(10,9));
        map.addObj(new Tree(11,10));
        map.addObj(new Tree(5,10));
        map.addObj(new Tree(0,0));
        map.addObj(new Tree(14,0));
        map.addObj(new Tree(0,14));
        map.addObj(new Tree(14,14)); 
        map.addObj(new Fuel(0, 5, 20));
        map.addObj(new Fuel(12, 12, 10));
        map.addObj(new Fuel(9, 6, 30));
        map.addObj(new Fuel(7, 14, 100));
        map.setRobot(robot); //Priority High
        map.setBomb(bomb); 
        while(true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if(direction=='q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
